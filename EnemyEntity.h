/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EnemyEntity.h
 * Author: samue
 *
 * Created on 3 de abril de 2019, 18:04
 */

#ifndef ENEMYENTITY_H
#define ENEMYENTITY_H

#include "Entity.h"
#include "Arena.h"

class EnemyEntity : public Entity {
public:
    EnemyEntity();
    EnemyEntity(const EnemyEntity& orig);
    virtual ~EnemyEntity();
    void SetPower(int power);
    int GetPower() const;
    void tryToAttackWithPower(Entity* ent);
    void SetArena(Arena* arena);
    Arena* GetArena() const;
private:
    int power;
    Arena* arena;

};

#endif /* ENEMYENTITY_H */

