/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Player.h
 * Author: samue
 *
 * Created on 2 de abril de 2019, 18:12
 */

#ifndef PLAYER_H
#define PLAYER_H

#include "Entity.h"
#include "Arena.h"

class Player : public Entity {
public:
    Player();
    Player(const Player& orig);
    virtual ~Player();
    void SetExp(int exp);
    int GetExp();
    void SetArena(Arena* arena);
    Arena* GetArena() const;
private:
    int exp;
    Arena* arena;

};

#endif /* PLAYER_H */

