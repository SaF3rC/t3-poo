/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: samue
 *
 * Created on 28 de marzo de 2019, 16:15
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>
#include <stdio.h>

#include "Entity.h"
#include "Arena.h"
#include "Player.h"

using namespace std;

void clear(){
    for(int i = 0; i<=40; i++){
        std::cout << std::endl;
    }
}

int main(int argc, char** argv) {
    

    std::cout << "----------[CONTROLES]----------" << std::endl;
    std::cout << "      Movimiento: W A S D" << std::endl;
    std::cout << "        Atacar: ESPACIO" << std::endl;
    std::cout << "        Acabar juego: C" << std::endl;
    std::cout << "-------------------------------" << std::endl;
    std::cout << " Presiona cualquier tecla para" << std::endl;
    std::cout << "     configurar la partida" << std::endl;

    system("stty raw"); 
    char input = getchar();
    system("stty cooked"); 
    clear();

    std::cout << "[CONFIGURACION] Escribe tu nombre: ";
    std::string nom;
    std::cin >> nom;
    
    char sym = 'Y';
    do{
        std::cout << "[CONFIGURACION] ¿Que simbolo quieres usar? ";
        std::cin >> sym;
        if(sym == 'Y') std::cout << "[ERROR] No puedes usar este simbolo" << std::endl;
    } while (sym == 'Y');
    
    int op = 0;
    do {
        std::cout << "[CONFIGURACION] Numero de oponentes: ";
        std::cin >> op;
    } while (op <= 0 && op >=20);
    
    Player* pers = new Player();
    
    pers->SetNombre(nom);
    Location* loc = pers->getLoc();
    loc->SetX(10);
    loc->SetY(10);
    pers->setLoc(loc);
    pers->setSym(sym);
        
    Arena* arena = new Arena(60, 20, nom);
    
    pers->SetArena(arena);
    
    arena->setEntity(pers, 0);
    
    op += 1;
    arena->setupEntities(op);
    
    bool end = false;
    do {
        arena->randomMoveEntities();
        arena->randomEntitiesAttack();
        arena->drawArena();
        end = arena->detectInput();
    } while (pers->GetVida() > 0 && end == false && arena->getNumEntities() > 1);
    
    clear();
    std::cout << "----------------------------------" << std::endl;
    std::cout << "    Has finalizado la partida" << std::endl;
    if(pers->GetVida() <= 0){
        std::cout << "       ¡Has sido asesinado!" << std::endl;
    } else if(arena->getNumEntities() <= 1){
        std::cout << "¡Has matado a todos tus oponentes!" << std::endl;
    }
    std::cout << "----------------------------------"  << std::endl;
    
    for(int i = 0; i<20; i++){
        Entity* ent = arena->getEntity(i);
        if(ent != nullptr){
            delete ent;
        }
    }
    
    delete arena;
    
    
}


