/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Arena.h
 * Author: samue
 *
 * Created on 28 de marzo de 2019, 16:25
 */

#ifndef ARENA_H
#define ARENA_H

#include <iostream>
#include <cstdlib>
#include <string>

#include "Entity.h"

class Arena {
public:
    Arena();
    Arena(int xSize, int ySize, std::string playerOne);
    Arena(const Arena& orig);
    virtual ~Arena();
    int getXSize();
    int getYSize();
    void setYSize(int ySize);
    void setXSize(int xSize);
    void drawArena();
    void setPlayerOne(std::string playerOne);
    std::string getPlayerOne() const;
    bool detectInput();
    void randomMoveEntities();
    void setupEntities(int op);
    void clear();
    Entity* getEntity(int i);
    void setEntity(Entity* ent, int i);
    void randomEntitiesAttack();
    int getNumEntities();
private:
    int xSize;
    int ySize;
    std::string playerOne;
    Entity* entities[20];
};

#endif /* ARENA_H */

