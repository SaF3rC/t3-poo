/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Personaje.cpp
 * Author: Samuel Fernandez
 * 
 * Created on 7 de marzo de 2019, 16:35
 */
#include <iostream>

#include "Entity.h"
#include "Player.h"
#include "Location.h"
#include "Arena.h"
#include "EnemyEntity.h"

Entity::Entity() {
    this->loc=new Location();
    this->nombre="Test";
    this->vida=20;
    this->sym='Y';
}

Entity::Entity(const Entity& orig) {
    std::cout << "No está implementado el construcctor por copia." << std::endl;
}

Entity::~Entity() {
    if (this->loc != nullptr){
        delete this->loc;
        this->loc = nullptr;
    }
}


void Entity::SetVida(int vida) {
    this->vida = vida;
}

int Entity::GetVida() {
    return vida;
}

void Entity::SetNombre(string nombre){
    this->nombre = nombre;
}

string Entity::GetNombre() {
    return nombre;
}


void Entity::setLoc(Location* loc) {
    this->loc = loc;
}

Location* Entity::getLoc() const {
    return loc;
}

Entity Entity::getEntity() {
    return *this;
    
}

void Entity::damage(int dam) {
    this->SetVida(GetVida()-dam);
}

void Entity::heal(int vida) {
    this->SetVida(GetVida()+vida);
}

void Entity::teleport( Location* loc) {
    this->loc = loc;
}


void Entity::attack(Entity* entities[], int dist) {
    bool victimFound = false;
    for(int i = 1; i<20; i++){
        Entity* ent = entities[i];
        
        Player* player = dynamic_cast<Player*>(entities[0]);
        
        if(ent != nullptr){
            if(isInAttackPositionWith(ent, dist)){
                int dam = (std::rand()%3)+1;
                ent->damage(dam); 
                victimFound = true;
                std::cout << std::endl << this->GetNombre() << " ha atacado a " << ent->GetNombre() << ". Daño: " << dam << std::endl;
                if(player != nullptr){
                    if(this->GetNombre() == player->GetArena()->getPlayerOne()){
                        player->SetExp(player->GetExp()+2);
                    }
                }
                if(ent->GetVida() < 0){
                    std::cout << ent->GetNombre() << " ha sido asesinado por " << this->GetNombre() << std::endl;
                    if(player != nullptr){
                        if(this->GetNombre() == player->GetArena()->getPlayerOne()){
                            player->SetExp(player->GetExp()+5);
                            player->SetVida(20);
                        }
                    }
                    delete ent;
                    entities[i] = nullptr;
                }
                break;
            }
        }
    }
    if(!victimFound) std::cout << std::endl << "No hay nadie a quien puedas atacar" << std::endl ;
}

bool Entity::isInAttackPositionWith(Entity* ent, int dist) {
    int x = ent->getLoc()->GetX() - this->getLoc()->GetX();
    if(x<=dist && x>=(-dist)){
        int y = ent->getLoc()->GetY() - this->getLoc()->GetY();
        if(y<=dist && y>=(-dist)){
            return true;
        }
    }
    return false;
    
    
}

void Entity::setSym(char sym) {
    this->sym = sym;
}

char Entity::getSym() const {
    return sym;
}

std::string Entity::getFancyHealth() {
    float progress = GetVida()/20.0;
    std::string health = "";
    int lenght = 20;
    int pos = progress * lenght;

    for(int i=0; i != lenght; ++i)
    {
        if(i < pos)
            health.append("♥");
        else
            health.append(" ");
    }
    return health;
}
