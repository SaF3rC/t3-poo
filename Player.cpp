/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Player.cpp
 * Author: samue
 * 
 * Created on 2 de abril de 2019, 18:12
 */

#include "Player.h"

Player::Player() {
    exp = 0;
    this->setSym('X');
}

Player::Player(const Player& orig) {
    this->exp = orig.exp;
    this->setSym('X');
}

Player::~Player() {
    delete this->getLoc();
    this->setLoc(nullptr);
    
}


void Player::SetExp(int exp) {
    this->exp = exp;
}

int Player::GetExp() {
    return exp;
}

void Player::SetArena(Arena* arena) {
    this->arena = arena;
}

Arena* Player::GetArena() const {
    return arena;
}
