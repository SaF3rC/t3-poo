/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Personaje.h
 * Author: Samuel Fernandez
 *
 * Created on 7 de marzo de 2019, 16:35
 */

#ifndef ENTITY_H
#define ENTITY_H

//#include <cstdlib>
//#include <string>
#include <iostream>

using std::string;

#include "Location.h"

class Entity {
public:
    Entity();
    Entity(const Entity& orig);
    virtual ~Entity();
    Entity getEntity();
    void SetVida(int vida);
    int GetVida();
    void SetNombre(string nombre);
    string GetNombre();
    void teleport(Location* loc);
    void damage(int dam);
    void heal(int vida);
    void setLoc(Location* loc);
    Location* getLoc() const;
    void attack(Entity* entities[], int dist);
    bool isInAttackPositionWith(Entity* ent, int dist);
    void setSym(char sym);
    char getSym() const;
    std::string getFancyHealth();
    
private:
    string nombre;
    int vida;
    Location* loc;
    char sym;
};

#endif /* PERSONAJE_H */

