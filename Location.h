/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Location.h
 * Author: Samuel Fernandez
 *
 * Created on 7 de marzo de 2019, 16:37
 */

#ifndef LOCATION_H
#define LOCATION_H

class Location {
public:
    Location();
    Location(const int x, const int y);
    Location(const Location& orig);
    virtual ~Location();
    Location* SetY(int y);
    int GetY() const;
    Location* SetX(int x);
    int GetX() const;
    Location* addX(int x);
    Location* addY(int y);
    bool operator==(const Location& right) const;
private:
    float x, y;

};

#endif /* LOCATION_H */

