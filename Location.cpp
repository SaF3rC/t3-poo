/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Location.cpp
 * Author: Samuel Fernandez
 * 
 * Created on 7 de marzo de 2019, 16:37
 */

#include "Location.h"

Location::Location() {
    this->x = 0;
    this->y = 0;
}

Location::Location(const int x, const int y){
    this->x = x;
    this->y = y;
}

Location::Location(const Location& orig) {
    this->x = orig.x;
    this->y = orig.y;
}

Location::~Location() {
}


Location* Location::SetY(int y) {
    this->y = y;
    return this;
}

int Location::GetY() const {
    return y;
}

Location* Location::SetX(int x) {
    this->x = x;
    return this;
}

int Location::GetX() const {
    return x;
}

bool Location::operator==(const Location& right) const {
    if (this->x == right.x && this->y == right.y){
        return true;
    }
    else {
        return false;
    }
}


Location* Location::addX(int x) {
    this->SetX(GetX()+x);
    return this;

}

Location* Location::addY(int y) {
    this->SetY(GetY()+y);
    return this;
}
