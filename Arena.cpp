/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Arena.cpp
 * Author: samue
 * 
 * Created on 28 de marzo de 2019, 16:25
 */

#include <cstdlib>
#include <iostream>
#include <sstream>

#include "Arena.h"
#include "EnemyEntity.h"
#include "Entity.h" 
#include "Player.h"

Arena::Arena() {
    this->xSize = 40;
    this->ySize = 20;
    for(int i = 0; i<20; i++){
        this->entities[i] = nullptr;
    }
    
}

Arena::Arena(int xSize, int ySize, std::string pl){
    this->xSize = xSize;
    this->ySize = ySize;
    this->playerOne = pl;
    for(int i = 0; i<20; i++){
        this->entities[i] = nullptr;
    }
}

Arena::Arena(const Arena& orig) {
    xSize=orig.xSize;
    ySize=orig.ySize;
    for(int i = 0; i<20; i++){
        this->entities[i] = nullptr;
        this->entities[i] = orig.entities[i];
    }
}

Arena::~Arena() {
    for(int i = 0; i<20; i++){
        this->entities[i] = nullptr;
    }
}

int Arena::getXSize() {
    return xSize;
}

int Arena::getYSize() {
    return ySize;
}

void Arena::setYSize(int ySize) {
    this->ySize = ySize;
}

void Arena::setXSize(int xSize) {
    this->xSize = xSize;
}

bool Arena::detectInput() {
    system("stty raw"); 
    char input = getchar();
    system("stty cooked"); 

    Entity* player1 = getEntity(0);
    Location* loc = player1->getLoc();
    switch(input){
        case 'w':
            loc->SetY(loc->GetY()-1);
            player1->setLoc(loc);
            return false;
        case 'a':
            loc->SetX(loc->GetX()-1);
            player1->setLoc(loc);
            return false;
        case 's':
            loc->SetY(loc->GetY()+1);
            player1->setLoc(loc);
            return false;
        case 'd':
            player1->setLoc(loc);
            loc->SetX(loc->GetX()+1);
            return false;
        case ' ':
            player1->attack(entities, 3);
            return false;
        case 'c':
            return true;
        default:
            return false;
    }

}

void Arena::randomMoveEntities() {
    for(int i = 0; i<20 ; i++){
        Entity* opers = getEntity(i);
        if(opers != nullptr && opers->GetNombre() != getPlayerOne()){
            Location* loc = opers->getLoc();
            if(loc->GetX() > getXSize()  || loc->GetX() < 0){
                loc->SetX(getXSize()/2);
            }
            if(loc->GetY() > getYSize() || loc->GetY() < 0){
                loc->SetY(getYSize()/2);
            }
            int move = rand() % 4;
            switch (move){
                case 0:
                    loc->SetX(loc->GetX()+1);
                    opers->setLoc(loc);
                    break;
                case 1:
                    loc->SetX(loc->GetX()-1);
                    opers->setLoc(loc);
                    break;
                case 2:
                    loc->SetY(loc->GetY()-1);
                    opers->setLoc(loc);
                    break;
                case 3:
                    loc->SetY(loc->GetY()+1);
                    opers->setLoc(loc);
                    break;
            }
        }
    }
}

void Arena::randomEntitiesAttack() {
    for(int i = 0; i<20 ; i++){
        EnemyEntity* enemy = dynamic_cast<EnemyEntity*>(getEntity(i));
        if(enemy != nullptr){
            enemy->tryToAttackWithPower(getEntity(0));
        }
    }
}

void Arena::setupEntities(int op) {
    for(int i = 1; i<20 && i<op; i++){
        EnemyEntity* pers = new EnemyEntity();
        std::stringstream ss;
        ss << "Personaje" << i;
        pers->SetNombre(ss.str());
        pers->setLoc(new Location(rand()%getXSize(), rand()%getYSize()));
        pers->SetArena(this);
        this->setEntity(pers, i);
    }
}

void Arena::drawArena() {
    
    std::string topborder;
    for(int j = 0; j<xSize+2; j++)  topborder.append("-");
    
    std::cout << topborder << std::endl;
    
    for(int i = 0; i<ySize; i++){
        std::cout << "|";
        for(int j = 0; j<xSize; j++) {
            bool drawEmpty = true;    
            for(Entity* pers : entities){
                if(pers != nullptr){
                    int x = pers->getLoc()->GetX();
                    int y = pers->getLoc()->GetY();
                    if(x >= xSize) {
                        Location* loc = pers->getLoc();
                        loc->SetX(xSize-1);
                        pers->setLoc(loc);
                    }
                    if(x < 0) {
                        Location* loc = pers->getLoc();
                        loc->SetX(0);
                        pers->setLoc(loc);
                    }
                    if(y >= ySize) {
                        Location* loc = pers->getLoc();
                        loc->SetY(ySize-1);
                        pers->setLoc(loc);
                    }
                    if(y <= 0) {
                        Location* loc = pers->getLoc();
                        loc->SetY(0);
                        pers->setLoc(loc);
                    }
                    if(pers->GetNombre() == playerOne){
                        if(i == y && j == x) {
                            std::cout << pers->getSym();
                            drawEmpty = false;
                        }
                    } else {
                        if(i == y && j == x) {
                            std::cout << pers->getSym();
                            drawEmpty = false;
                        }
                    }
                }
                
            }
            if(drawEmpty) std::cout << " ";
            
            
        }
        std::cout << "|" << std::endl;
    }
    
    std::cout << topborder << std::endl;
    Player* pers = dynamic_cast<Player*>(entities[0]);
    std::cout << "| " << pers->GetNombre() << " | Exp: " << pers->GetExp() << " | " << pers->getFancyHealth() << std::endl << std::endl << std::endl << std::endl;
  
}

void Arena::setPlayerOne(std::string playerOne) {
    this->playerOne = playerOne;
}

std::string Arena::getPlayerOne() const {
    return playerOne;
}

void Arena::clear(){
    for(int i = 0; i<=40; i++){
        std::cout << std::endl;
    }
}

Entity* Arena::getEntity(int i){
    return this->entities[i];
}

void Arena::setEntity(Entity* ent, int i) {
    this->entities[i] = ent;
}

int Arena::getNumEntities() {
    int cont = 0;
    for(int i = 0; i<20; i++){
        if(getEntity(i) != nullptr){
            ++cont;
        }
    }
    return cont;
}

