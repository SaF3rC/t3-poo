/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EnemyEntity.cpp
 * Author: samue
 * 
 * Created on 3 de abril de 2019, 18:04
 */

#include "EnemyEntity.h"

EnemyEntity::EnemyEntity() {
    this->setSym('Y');
    this->power = 1;
}

EnemyEntity::EnemyEntity(const EnemyEntity& orig) {
    this->power = orig.power;
}

EnemyEntity::~EnemyEntity() {
}

void EnemyEntity::SetPower(int power) {
    this->power = power;
}

int EnemyEntity::GetPower() const {
    return power;
}

void EnemyEntity::tryToAttackWithPower(Entity* ent) {
    int rand = std::rand()%100;
    if(isInAttackPositionWith(ent, 2)){
        if(rand < 50){
            if(rand < 10){
                ent->damage(this->GetPower()*3);
                std::cout << "¡Golpe critico! " << this->GetNombre() << " ha hecho " << this->GetPower()*3 << " puntos de daño a " << ent->GetNombre() << std::endl;
            } else {
                ent->damage(this->GetPower());
                std::cout << this->GetNombre() << " ha hecho " << this->GetPower()*3 << " puntos de daño a " << ent->GetNombre() << std::endl;
            }
        }
    }
}

void EnemyEntity::SetArena(Arena* arena) {
    this->arena = arena;
}

Arena* EnemyEntity::GetArena() const {
    return arena;
}
